package com.zy.single;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class LazyMan {
    private LazyMan() {
        System.out.println(Thread.currentThread().getName() + "ok");

    }

    private volatile static LazyMan lazyMan;

    public static LazyMan getInstance() {

        if (lazyMan == null) {
            synchronized (LazyMan.class) {
                lazyMan = new LazyMan();
            }
        }
        return lazyMan;
    }

    public static void main(String[] args) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        LazyMan instance = LazyMan.getInstance();
        Constructor<LazyMan> declaredConstructor = LazyMan.class.getDeclaredConstructor(null);
        declaredConstructor.setAccessible(true);
        LazyMan instance2 = declaredConstructor.newInstance();
        System.out.println(instance);
        System.out.println(instance2);

    }

}
