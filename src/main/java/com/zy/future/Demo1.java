package com.zy.future;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

public class Demo1 {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
//        CompletableFuture<Void> completableFuture =CompletableFuture.runAsync(()->{
//            try {
//                TimeUnit.SECONDS.sleep(2);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            } finally {
//                System.out.println(Thread.currentThread().getName()+"runAsync=>Void");
//            }
//        });
//        System.out.println("111111");
//        completableFuture.get();
        CompletableFuture<Integer> completableFuture =CompletableFuture.supplyAsync(()->{
            System.out.println(Thread.currentThread().getName()+"supplyAsync=>Integer");
            return  1024;
        });
        completableFuture.whenComplete((t,u)->{
            System.out.println("t=>"+t);
            System.out.println("u=>"+u);
        }).exceptionally((e)->{
            System.out.println(e.getMessage());
            return 233;
        }).get();
    }
}
