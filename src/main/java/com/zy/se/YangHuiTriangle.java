package com.zy.se;

/**
 * 前提：每行端点与结尾的数为1.
 * 每个数等于它上方两数之和。
 * 每行数字左右对称，由1开始逐渐变大。
 * 第n行的数字有n项。
 * 前n行共[(1+n)n]/2 个数。
 * 第n行的m个数可表示为 C(n-1，m-1)，即为从n-1个不同元素中取m-1个元素的组合数。
 * 第n行的第m个数和第n-m+1个数相等 ，为组合数性质之一。
 */
public class YangHuiTriangle {
    public static void main(String[] args) {
        //声明并初始化二维数组;
        int[][] yangHui = new int[10][];
        for (int i = 0; i < yangHui.length; i++) {
            yangHui[i] = new int[i + 1];
        }
        //给二维数组赋值
        for (int i = 0; i < yangHui.length; i++) {
            //先给边上的赋值
            yangHui[i][0] = 1;
            yangHui[i][i] = 1;
            //再给中间的赋值
            for (int j = 1; j < yangHui[i].length - 1; j++) {
                yangHui[i][j] = yangHui[i - 1][j - 1] + yangHui[i - 1][j];
            }
        }
        //遍历二维数组
        for (int i = 0; i < yangHui.length; i++) {
            for (int j = 0; j < yangHui[i].length; j++) {
                System.out.print(yangHui[i][j] + " ");
            }
            System.out.println();
        }
    }
}