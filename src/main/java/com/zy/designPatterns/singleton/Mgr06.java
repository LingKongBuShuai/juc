package com.zy.designPatterns.singleton;

/**
 * 饿汉式（为什么要加载实例）--懒汉式（线程不安全）-加锁（效率降低）-加锁（静态代码块，不起作用）-双重校验（再加一个if）--静态内部类（JVM保证安全，类似01）
 * lazy loadidng
 * 也称懒汉式
 * 虽然达到初始化的目的，但却带来线程不安全的问题
 * 多线程时，线程不安全，因为线程1判断为空时，去new实例的时候（实例尚未初始化/可以让线程睡几秒测试），
 * 此时来了线程2，他也会判断线程为空，再次去new实例，依次类推
 */
public class Mgr06 {
    //为什么不加final final的话必须初始化（final：属性，值不可修改（我都还没有值，怎么改？），类不可继承）
    //volatile:禁止JIT指令重排序
    private static volatile Mgr06 Instance;
    private Mgr06(){

    };
    //加锁 ：效率会降低 ，锁的是class Mgr04这个对象
    public static Mgr06 getInstance(){
        //判空--上锁--判空
        if (Instance==null){
            //双重检查
            synchronized(Mgr06.class){
                if (Instance==null){

//            try {
//                Thread.sleep(1);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
                    Instance = new Mgr06();
                }
            }
        }
        return Instance;
    }
    public void m(){
        System.out.println("m");
    }

    public static void main(String[] args) {
        //如何验证多线程不安全，new100个线程试一试
        //不同对象的hash码是不一样的；同一个对象的hash码是一样的；hash码相同，对象不一定一样（hash碰撞）
        for (int i = 0; i < 100; i++) {
            //lamda表达式：runnable接口里只有一个run（）方法
            new Thread(()->{
                System.out.println(Mgr06.getInstance().hashCode());
            }).start();
        }
    }

}
