package com.zy.ex;

import java.util.Arrays;

public class SelectSortDemo {
    public static void main(String[] args) {
        int[] arr = {8, 7, 9, 5, 2, 1, 3, 6, 4};
        for (int i = 0; i < arr.length; i++) {
            int minpos=i;
            for (int j = i+1; j < arr.length; j++) {
              minpos= arr[j]<arr[minpos]?j:minpos;
            }
            int temp =arr[i];
            arr[i]=arr[minpos];
            arr[minpos]=temp;
        }
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i]+"");
        }
    }
}
