package com.zy.designPatterns.singleton;

/**
 * 饿汉式（为什么要加载实例）--懒汉式（线程不安全）-加锁（效率降低）-加锁（静态代码块，不起作用）-双重校验
 * lazy loadidng
 * 也称懒汉式
 * 虽然达到初始化的目的，但却带来线程不安全的问题
 * 多线程时，线程不安全，因为线程1判断为空时，去new实例的时候（实例尚未初始化/可以让线程睡几秒测试），
 * 此时来了线程2，他也会判断线程为空，再次去new实例，依次类推
 */
public class Mgr05 {
    //为什么不加final final的话必须初始化（final：属性，值不可修改（我都还没有值，怎么改？），类不可继承）
    private static Mgr05 Instance;
    private Mgr05(){

    };
    //加锁 ：效率会降低 ，锁的是class Mgr04这个对象
    public static  Mgr05 getInstance(){
        if (Instance==null){
            //快捷键 ctrl+alt+T
            //妄图通过减小同步代码块的方式提高效率，然后不可行
            //if判断不在锁内，依然会持续判空，在new出对象，释放锁后，新的线程已在门外，此时if判断有值的条件，尚未返回，导致新的线程进入拿到锁，从而没有起到作用
            synchronized(Mgr05.class){

//            try {
//                Thread.sleep(1);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
                Instance = new Mgr05();
            }
        }
        return Instance;
    }
    public void m(){
        System.out.println("m");
    }

    public static void main(String[] args) {
        //如何验证多线程不安全，new100个线程试一试
        //不同对象的hash码是不一样的；同一个对象的hash码是一样的；hash码相同，对象不一定一样（hash碰撞）
        for (int i = 0; i < 100; i++) {
            //lamda表达式：runnable接口里只有一个run（）方法
            new Thread(()->{
                System.out.println(Mgr05.getInstance().hashCode());
            }).start();
        }
    }

}
