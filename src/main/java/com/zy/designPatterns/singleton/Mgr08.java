package com.zy.designPatterns.singleton;

/**
 * Effective Java
 * 不仅可以解决线程同步，还可以防止反序列化
 * 枚举单例：利用枚举的唯一取值（perfect）
 * 枚举类不会被反序列化的原因是，它能被正确的加锁、同步
 */
public enum Mgr08 {
    INSTANCE;
    public  void m(){}

    public static void main(String[] args) {
        for (int i = 0; i < 100; i++) {
            new Thread(()->{
                System.out.println(Mgr08.INSTANCE.hashCode());
            }).start();
        }
    }
}
