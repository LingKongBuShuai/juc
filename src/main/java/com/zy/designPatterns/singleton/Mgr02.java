package com.zy.designPatterns.singleton;

/**
 * 饿汉式
 * 类加载到内存后，就实例化一个单例，JVM保证线程安全（保证一个类只加载一次）
 * 简单实用，推荐使用
 * 唯一缺点：不管用到与否，类加载时就已经完成了实例化
 * Class.forName("")--只加载到内存，不实例化
 * （就是你明明不用它/实例，你加载它干嘛？）
 * 跟01一样，本质没区别，只是把实例化放到了----静态代码块--
 */
public class Mgr02 {
    //static 保证只有一个实例
    private static final Mgr02 Instance;
    static {
        Instance =new Mgr02();
    }
    //构造方法设置为私有private，保证其他人new不出来，只能调用static里的实例
    private Mgr02(){}
    public static Mgr02 getInstance(){return Instance;}
    public void  m(){
        System.out.println("m");
    }

    public static void main(String[] args) {
        Mgr02 m1 = Mgr02.getInstance();
        Mgr02 m2 = Mgr02.getInstance();
        //如何证明只有一个单例：true:只有一个地址--指向同一个引用地址
        System.out.println(m1==m2);
    }
}
