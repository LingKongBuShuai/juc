package com.zy.stream;

import com.sun.xml.internal.ws.api.model.wsdl.WSDLOutput;

import java.util.Arrays;
import java.util.List;

public class Test {
    public static void main(String[] args) {
        User u1 = new User(1, "a", 21);
        User u2 = new User(1, "b", 22);
        User u3 = new User(1, "c", 23);
        User u4 = new User(1, "d", 24);
        List<User> list = Arrays.asList(u1, u2, u3, u4);
        list.stream().filter(u -> {return u.getId()%2==0;  })
                .filter(u->{return  u.getAge()>22;})
                .map(u->{return u.getName().toUpperCase();})
                .sorted((uu1,uu2)->{return uu1.compareTo(uu2);})
                .limit(1)
                .forEach(System.out::println);
    }
}
