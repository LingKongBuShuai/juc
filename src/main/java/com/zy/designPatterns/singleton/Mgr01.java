package com.zy.designPatterns.singleton;

/**
 * 饿汉式（为什么要加载实例、放入静态代码块）--懒汉式
 * 饿汉式
 * 类加载到内存后，就实例化一个单例，JVM保证线程安全（保证一个类只加载一次）
 * 简单实用，推荐使用
 * 唯一缺点：不管用到与否，类加载时就已经完成了实例化
 * Class.forName("")--只加载到内存，不实例化
 * （就是你明明不用它/实例，你加载它干嘛？）
 */
public class Mgr01 {
    //static 保证只有一个实例
    private static final  Mgr01 Instance =new Mgr01();
    //构造方法设置为私有private，保证其他人new不出来，只能调用static里的实例
    private Mgr01(){}
    public static Mgr01 getInstance(){return Instance;}
    public void  m(){
        System.out.println("m");
    }

    public static void main(String[] args) {
        Mgr01 m1 = Mgr01.getInstance();
        Mgr01 m2 = Mgr01.getInstance();
        //如何证明只有一个单例：true:只有一个地址--指向同一个引用地址
        System.out.println(m1==m2);
    }
}
