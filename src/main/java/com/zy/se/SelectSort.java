package com.zy.se;

public class SelectSort {
    public int[] sort(int arr[]) {
        int temp = 0;
        for (int i = 0; i < arr.length - 1; i++) {
            //认为目前的数就是最小的，记录最小数的下标
            int minIndex = i;
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[minIndex] > arr[j]) {//修改最小值的下标
                    minIndex = j;

                }
            }//当退出for就找到这次的最小值，就需要交换位置
            if (i != minIndex) {
                temp = arr[i];
                arr[i] = arr[minIndex];
                arr[minIndex] = temp;
            }
        }
        return arr;
    }

    public static void main(String[] args) {
        SelectSort selectSort = new SelectSort();
        int array[] = {5, 81, 69, 1, 91, 8, 4};
        int sort[] = selectSort.sort(array);
        for (int num : sort) {
            System.out.println(num + "\t");
        }
    }
}
