package com.zy.se;

public class Bubble {


    public int[] sort(int[] array) {
        int temp = 0;
        //外层循环，它决定一共走几趟  //-1为了防止溢出
        for (int i = 0; i < array.length - 1; i++) {
            int flag = 0;//定义一个符号位 减少无谓的比较，如果已经有序，就退出循环
            //内层循环，它决定每趟走几次
            for (int j = 0; j < array.length - i - 1; j++) {
                if (array[j + 1] > array[j]) {
                    temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                    flag = 1;
                }
            }
            if (flag == 0) {
                break;
            }
        }
        return array;
    }
    public static void main(String[] args) {
        Bubble bubble =new Bubble();
        int[] array ={5,6,2,924,1,81,6,};
        int [] sort =bubble.sort(array);
        for(int num :sort){
            System.out.println(num+"\t");
        }
    }


}
