package com.zy.forkjoin;

import java.util.concurrent.RecursiveTask;
import java.util.concurrent.locks.Lock;

public class ForkJoinDemo extends RecursiveTask<Long> {
    private Long start;
    private Long end;

    private Long temp = 10000L;

    public ForkJoinDemo(Long start, Long end) {
        this.start = start;
        this.end = end;
    }


    @Override
    protected Long compute() {
        if ((end-start)<temp){
            Long sum =0L;
            for (Long i = start; i <=end; i++) {
                sum +=i;
            }
            System.out.println(sum);
        }else{
            long middle =(start+end)/2;
            ForkJoinDemo task1 = new ForkJoinDemo(start, middle);
            task1.fork();
            ForkJoinDemo task2 = new ForkJoinDemo( middle,end);
            task2.fork();
            return task1.join()+task2.join();


        }
        return null;
    }
}
