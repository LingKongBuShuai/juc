package com.zy.pool;


import java.util.concurrent.*;

public class Demo1 {
    public static void main(String[] args) {

        //最大线程到底该如何定义
        //CPU 密集型 电脑是几核
        //IO 密集型
        //获取CPU的核数
        System.out.println(Runtime.getRuntime().availableProcessors());
        ThreadPoolExecutor threadPool = new ThreadPoolExecutor(2,
                5,
                3,
                TimeUnit.SECONDS,
                new LinkedBlockingDeque<>(3),
                Executors.defaultThreadFactory(),
                new ThreadPoolExecutor.AbortPolicy());
        //ExecutorService threadPool = Executors.newSingleThreadExecutor();
        //  ExecutorService threadPool =Executors.newFixedThreadPool(5);
         // ExecutorService threadPool =Executors.newCachedThreadPool();


            try {
                for (int i = 0; i < 7; i++) {
                    threadPool.execute(() -> {
                        System.out.println(Thread.currentThread().getName() + "ok");
                    });
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                threadPool.shutdown();
            }
        }
    }

