package com.zy.designPatterns.strategy;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Cat [] a={new Cat(3,3),new Cat(5,5),new Cat(1,1)};
        Sorter sorter =new Sorter();
        sorter.sort(a);
        System.out.println(Arrays.toString(a));
//        int [] a={7,9,5,2,3,4};
//        Sorter sorter =new Sorter();
//        sorter.sort(a);
//        System.out.println(Arrays.toString(a));
    }
}
