package com.zy.designPatterns.strategy;

public class Sorter {
    //选择排序--这里只能传入int类型 如何优化恩？
    public static void sort(Cat [] arr){
        for (int i = 0; i < arr.length-1; i++) {
            int minPos = i;
            for (int j = i+1; j <arr.length ; j++) {
                //这里的i（int minPos = i; j = i+1），正常情况下j是一直大于i的
                //也就是说，当后面的数小于前面的数，交换角标换位，否则循环判断，三元表达式赋值后，minPos永远是较大的那个数
                minPos=arr[j].compareTo(arr[minPos])==1 ? j:minPos;

            }
            swap(arr,i,minPos);

        }
    }
    static void swap(Cat[] arr,int i ,int j){
        Cat temp=arr[i];
        arr[i]=arr[j];
        arr[j]=temp;
    }
//      public static void sort(int [] arr){
//    for (int i = 0; i < arr.length-1; i++) {
//            int minPos = i;
//            for (int j = i+1; j <arr.length ; j++) {
//                //这里的i（int minPos = i; j = i+1），正常情况下j是一直大于i的
//                //也就是说，当后面的数小于前面的数，交换角标换位，否则循环判断，三元表达式赋值后，minPos永远是较大的那个数
//                minPos=arr[j]<arr[minPos]?j:minPos;
//
//            }
//            swap(arr,i,minPos);
//
//        }
//    }
//    static void swap(Cat[] arr,int i ,int j){
//        int temp=arr[i];
//        arr[i]=arr[j];
//        arr[j]=temp;
//    }
}
