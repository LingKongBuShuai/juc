package com.zy.se;

public class RhombusPrint {
    public void print1(){
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j <= (5 - i); j++) {
                System.out.print(" ");
            }
            for (int k = 1; k <= (2 * i - 1); k++) {
                System.out.print("*" );
            }
            System.out.println();
        }
    }
    public void print2(){
        for (int i = 5; i >0 ; i--) {
            for (int j = 0; j <=(5 - i); j++) {
                System.out.print(" ");
            }
            for (int k = 1; k<= (2 * i - 1); k++) { //2i-1 打印奇数
                System.out.print("*");
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {
        RhombusPrint print =new RhombusPrint();
        print.print1();
        print.print2();
    }
}
