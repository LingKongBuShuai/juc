package com.zy.lock8;

import java.util.concurrent.TimeUnit;

/**
 *
 */
public class Test3 {
    public static void main(String[] args) {
       Phone3 phone1 =new Phone3();
       Phone3 phone2 =new Phone3();
        new Thread(()->{
            phone1.sendSms();
        },"A").start();
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        new Thread(()->{
            phone2.call();
        },"B").start();

    }
}
class Phone3{
    public static synchronized  void sendSms(){
        //sychronized 锁的是对象的调用者
        //static 静态方法
        //类一加载就有了，锁的是class
        try {
            TimeUnit.SECONDS.sleep(4);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("发短信");
    }
    public static   synchronized  void  call(){
        System.out.println("打电话");
    }

}