package com.zy.function;

import java.util.function.Supplier;

public class Demo4 {
    public static void main(String[] args) {
//        Supplier<Integer> supplier = new Supplier<Integer>() {
//            @Override
//            public Integer get() {
//                return 1024;
//            }
//        };
        Supplier<Integer> supplier =()->{return 1024;};
        System.out.println(supplier.get());
    }
}
